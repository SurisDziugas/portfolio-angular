import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { CardComponent } from './card.component';
import { Post } from '../new';

@Component({
  template: `<app-card [post]="post"></app-card>`
})
class TestComponent extends CardComponent {
  post = { title: 'title' } as Post;
}

describe('CardComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardComponent, TestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('renamePost ->', () => {
    fit('should change post name', () => {
      component.renamePost();

      expect(component.post.title).toBe('titlename');
    });
  });

});
