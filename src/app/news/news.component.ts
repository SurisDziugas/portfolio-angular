import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Post } from '../new';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})


export class NewsComponent implements OnInit {
  title = 'news';
  posts: Post[];

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.getNews();
    console.log(this.posts);
  }

  getNews(): void {
    this.newsService.getPosts().subscribe(posts => {
      this.posts = posts;
    });
  }



}
